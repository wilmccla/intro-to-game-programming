﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovemet : MonoBehaviour
{
    // Declare an animator in order to use animations
    public Animator animator;
   
    // Declaring the Variables
    public float moveSpeed = 3f;
    public float jumpSpeed = 3f;
    public float velocityX;
    public float velocityY;
    public bool facingRight = true;
    public Rigidbody2D rigidBody;
    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask theGround;

    
	// Use this for initialization
	void Start ()
	{//
	    rigidBody = GetComponent<Rigidbody2D>();
	    // rigidBody is able to get velocity values in order to move the sprite
    }

    // Update is called once per frame
    void Update ()
	{// Getting input from the player. Horizontal class applies to both A and D on the keyboard, as well as the left and right arrowkeys.
        if (!Input.GetKey(KeyCode.LeftShift)) velocityX = Input.GetAxisRaw("Horizontal");
        velocityY = rigidBody.velocity.y;
        if (!Input.GetKey(KeyCode.LeftShift)) rigidBody.velocity = new Vector2(velocityX * moveSpeed, velocityY);

	    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.LeftArrow)) // if user presses both leftshift and the left arrow
	    {
	        Vector2 position = transform.position;
	        position.x--;
	        transform.localPosition = position; // the sprite will move 1 unit to the left

	    }

	    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.RightArrow)) // if user presses both leftshift and right arrow
	    {
	        Vector2 position = transform.position;
	        position.x++;
	        transform.localPosition = position; // the sprite will move 1 unit to the right
	    }

        // Give the animator a variable to change in order to transition from an idle to a running animation
        animator.SetFloat("Speed", Mathf.Abs(velocityX));
        if (Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetFloat("Speed", Mathf.Abs((velocityX * 0)));
        }
        
        // Give the player the ability to jump
	    isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, theGround); //Checks if the player is touching the ground
        animator.SetBool("isGrounded", isGrounded); // Gives the animator a parameter to change animations
	    if (isGrounded == true && Input.GetKeyDown(KeyCode.W) || isGrounded == true && Input.GetKeyDown(KeyCode.UpArrow)) // If player is on the ground, and presses W or the Up Arrow...
	    {
            rigidBody.velocity = Vector2.up * jumpSpeed; // ... they will jump
	    }

	    if (Input.GetKeyDown(KeyCode.Space)) // If user presses space...
	    {
	        transform.localPosition = new Vector3(-12, -1, -1); // ... player will be moved to position x=-12 y=-1 z=-1
	    }
    }

    void LateUpdate()
    {// Determining which way the sprite is moving, and changing the X scale to match the direction. Right is positive, Left is Negative.
        Vector3 scaleX = transform.localScale;
        if (velocityX > 0) // If the velocityX of the player is positive....
        {
            facingRight = true;
        } else if (velocityX < 0) // ... the player is facing right
        {
            facingRight = false;
        }

        if (((facingRight) && (scaleX.x < 0)) || ((!facingRight) && (scaleX.x > 0)))
            scaleX.x *= -1;
        transform.localScale = scaleX;
        // This part actually takes the X scale for the sprite and multiplies it by -1, or in other words causes the sprite to flip horizontally
    }
    
}
