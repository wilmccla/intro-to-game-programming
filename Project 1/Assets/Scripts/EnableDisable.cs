﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisable : MonoBehaviour
{   
    //declaring variables
    public Behaviour movementComponent;
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) // If user presses P
        {
            movementComponent.enabled = !movementComponent.enabled; // movementComponent is inversed
        }

        if (Input.GetKeyDown(KeyCode.Q)) // if user presses q
        {
            Player.SetActive(false); // game object isnt active anymore and everything breaks
        }

        if (Input.GetKeyDown(KeyCode.Escape)) // if user presses escape
        {
            Application.Quit(); // the application will close
        }
    }
}
